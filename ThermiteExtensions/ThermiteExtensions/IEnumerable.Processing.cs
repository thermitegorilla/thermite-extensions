﻿namespace Thermite.Extensions
{
   using System;
   using System.Collections.Generic;

   /// <summary>
   /// Extension methods for processing System.Collections.Generic.IEnumerable<T>.</>
   /// </summary>
   public static partial class IEnumerable
   {
      /// <summary>
      /// Process a collection through an action is batches.
      /// </summary>
      /// <typeparam name="TSource">The type of the elements of source.</typeparam>
      /// <param name="source">An System.Collections.Generic.IEnumerable<T> to process.</param>
      /// <param name="action">Action to perform on the collection.</param>
      /// <param name="count">Size of batch to process per iteration.</param>
      public static void BatchProcess<TSource>(this IEnumerable<TSource> source, Action<IEnumerable<TSource>> action, int count)
      {
         if (source == null)
            throw new ArgumentNullException("Source cannot be null");

         if (action == null)
            throw new ArgumentNullException("Action must not be null");

         if (count <= 0)
            throw new ArgumentOutOfRangeException("Count must be greater than 0");

         foreach (IEnumerable<TSource> batch in source.Next(count))
         {
            action.Invoke(batch);
         }
      }
   }
}

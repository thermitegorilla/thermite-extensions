﻿namespace Thermite.Extensions
{
   using System;
   using System.Collections.Generic;
   using System.Linq;

   /// <summary>
   /// Extension methods for iterating over System.Collections.Generic.IEnumerable<T>.</>
   /// </summary>
   public static partial class IEnumerable
   {
      /// <summary>
      /// 
      /// </summary>
      /// <typeparam name="TSource"></typeparam>
      /// <param name="source"></param>
      /// <returns></returns>
      public static IEnumerable<TSource> Next<TSource>(this IEnumerable<TSource> source)
      {
         if (source == null)
            throw new ArgumentNullException("Source cannot be null");

         for (int i = 0; i < source.Count(); i++)
         {
            yield return source.Skip(i).First();
         }
      }

      /// <summary>
      /// 
      /// </summary>
      /// <typeparam name="TSource"></typeparam>
      /// <param name="source"></param>
      /// <param name="start"></param>
      /// <param name="end"></param>
      /// <returns></returns>
      public static IEnumerable<TSource> Next<TSource>(this IEnumerable<TSource> source, int start, int end)
      {
         if (source == null)
            throw new ArgumentNullException("Source cannot be null");

         if (start < 0 || start >= source.Count())
            throw new ArgumentOutOfRangeException("Start index must be greater than 0 and less than the size of the IEnumerable<T>");

         if (end < start || end > source.Count())
            throw new ArgumentOutOfRangeException("End index must be greater than the start index and not greater than the size of the IEnumerable<T>");

         for (int i = start; i < end; i++)
         {
            yield return source.Skip(i).First();
         }
      }

      /// <summary>
      /// Iterates through an IEnumerable<T> returning a sub-IEnumerable<T> of the given size.
      /// </summary>
      /// <typeparam name="TSource">The type of the elements of source.</typeparam>
      /// <param name="source">An System.Collections.Generic.IEnumerable<T> to iterate through.</param>
      /// <param name="count">The size of IEnumerable<T> to return.</param>
      /// <returns>Subcollection of the source IEnumerable<T>.</returns>
      public static IEnumerable<IEnumerable<TSource>> Next<TSource>(this IEnumerable<TSource> source, int count)
      {
         if (source == null)
            throw new ArgumentNullException("Source cannot be null");

         if (count <= 0)
            throw new ArgumentOutOfRangeException("Count must be greater than 0");

         for (int i = 0; i < source.Count(); i += count)
         {
            yield return source.Skip(i).Take(count);
         }
      }

      /// <summary>
      /// 
      /// </summary>
      /// <typeparam name="TSource"></typeparam>
      /// <param name="source"></param>
      /// <returns></returns>
      public static IEnumerable<TSource> ReverseNext<TSource>(this IEnumerable<TSource> source)
      {
         if (source == null)
            throw new ArgumentNullException("Source cannot be null");

         IEnumerable<TSource> reverse = source.Reverse();
         for (int i = 0; i < reverse.Count(); i++)
         {
            yield return reverse.Skip(i).First();
         }
      }

      /// <summary>
      /// 
      /// </summary>
      /// <typeparam name="TSource"></typeparam>
      /// <param name="source"></param>
      /// <param name="start"></param>
      /// <param name="end"></param>
      /// <returns></returns>
      public static IEnumerable<TSource> ReverseNext<TSource>(this IEnumerable<TSource> source, int start, int end)
      {
         if (source == null)
            throw new ArgumentNullException("Source cannot be null");

         if (end < 0 || end >= source.Count())
            throw new ArgumentOutOfRangeException("End index must be greater than 0 and less than the size of the IEnumerable<T>");

         if (start < end || start > source.Count())
            throw new ArgumentOutOfRangeException("Start index must be greater than the end index and not greater than the size of the IEnumerable<T>");

         IEnumerable<TSource> reverse = source.Reverse();
         int newEnd = (source.Count() - 1) - end;
         int newStart = (source.Count() - 1) - start;

         for (int i = newStart; i < newEnd; i++)
         {
            yield return reverse.Skip(i).First();
         }
      }

      /// <summary>
      /// 
      /// </summary>
      /// <typeparam name="TSource">The type of the elements of source.</typeparam>
      /// <param name="source">An System.Collections.Generic.IEnumerable<T> to iterate through.</param>
      /// <param name="count">The size of sub-IEnumerable<T> to return.</param>
      /// <returns>Subcollection of the source IEnumerable<T>.</returns>
      public static IEnumerable<IEnumerable<TSource>> ReverseNext<TSource>(this IEnumerable<TSource> source, int count)
      {
         if (source == null)
            throw new ArgumentNullException("Source cannot be null");

         if (count <= 0)
            throw new ArgumentOutOfRangeException("Count must be greater than 0");

         IEnumerable<TSource> reverse = source.Reverse();
         for (int i = 0; i < reverse.Count(); i += count)
         {
            yield return reverse.Skip(i).Take(count);
         }
      }
   }
}
